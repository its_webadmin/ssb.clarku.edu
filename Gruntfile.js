(function () {
  'use strict';

  module.exports = function (grunt) {
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      banner: '/*!\n' +
              ' * <%= pkg.name %> v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
              ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>\n' +
              ' * Licensed under <%= pkg.license %> (<%= pkg.homepage %>/blob/master/LICENSE)\n' +
              ' */\n',

      less: {
        screen: {
          files: {
            'dist/css/custom.css': 'src/less/ssb.less'
          },
          options: {
            banner: '<%= banner %>',
            compress: true,
            strictMath: true,
            strictUnits: true
          }
        }
      },
      shell: {
        options: { stderr: false },

        scp: { command: 'scp dist/css/custom.css <%= pkg.scp_dest %>' }
      },
      watch: {
        less: {
          files: ['src/less/**/*.less'],
          tasks: ['less', 'shell:scp']
        }
      }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
  };
}());