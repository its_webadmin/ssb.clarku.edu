# ssb.clarku.edu

This is a complete CSS replacement for Clark University's Self-Service Banner implementation.

## Banner Changes

In the `twbkwbis` package within `PROCEDURE p_opendoc ()` add the following:

    HTP.p('<link rel="stylesheet" href="/css/custom.css">');
    HTP.p('<meta name="viewport" content="width=device-width, initial-scale=1">');

I also renamed the CSS files in `css/` so they wouldn't be served. They can likely be removed programatically by changing a package as above, I just didn't know where to look.

## Dependencies

This project relies on [node.js](http://nodejs.org/). Once it's installed you'll need to install the Grunt CLI and Bower globally as well with `npm`:

    npm install -g grunt-cli
    npm install -g bower

After that, you'll need to get the Grunt plugins and Bower components for the project:

    grunt install
    bower install

## Deploying

To deploy, ensure the `scp_dest` in the `package.json` file is correct, this is where the `custom.css` file will be deployed to. After that, just run:

    grunt less

Since it's using SCP to deplay `custom.css`, you'll be prompted to authenticate with the server the file is being copied to.

## CSS Framework

The CSS is built with [Bootstrap](http://getbootstrap.com/). In a nutshell: Boostrap is fetched via Bower, imported piecemeal in `src/less/bootstrap.less` (complete with variable and grid overrides), and compiled into CSS. Because we're not looking to touch the underlying HTML all of the Bootstrap components are hooked into tags, IDs or classes that already exist in the markup. In a few cases (as with the descendant selectors of `.nav()`), copying less over from Bootstrap and modifying selectors was necessary; the markup in Banner didn't match what Bootstrap required.

## Watching for Changes

Running `grunt watch` will watch all files in `src/less` for changes, and upon a change re-compile the CSS and SCP it to where it needs to be.

*[CLI]: Command Line Interface
*[CSS]: Cascading Style Sheets
*[HTML]: HyperText Markup Language
*[SCP]: Secure Copy